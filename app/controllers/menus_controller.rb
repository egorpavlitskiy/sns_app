class MenusController < ApplicationController
  def index
    @user = current_user
    @week_menus = @user.week_menus
    @active_menu = @user.active_menu
  end
end
