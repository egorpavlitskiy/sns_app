class MessesController < ApplicationController
  def new
    @mess = Mess.new
    @ingredients = @mess.ingredients.build
    # @ingredients_counts = @messes.ingredients_counts.build
  end

  def show
    @mess = Mess.find(params[:id])
    render layout: false
  end

  def create
    Mess.create(mess_params)
    redirect_to action: 'new', controller: 'messes'
  end

  private

  def mess_params
    params.require(:mess).permit(:name,
                                   :description,
                                   :category,
                                   :cooking_time, ingredient_ids: [])
  end
end
