class WeekMenu < ActiveRecord::Base
  has_many :menus, -> {order_by_week_day.order_by_position}
  belongs_to :user
  belongs_to :active_menu
end
