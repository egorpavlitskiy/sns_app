class Mess < ActiveRecord::Base
  has_many :ingredients_counts
  has_many :menus
  has_many :ingredients, through: :ingredients_counts
  accepts_nested_attributes_for :ingredients_counts

  def nutrition_count type
    r = 0
    ingredients_counts.each do |i|
      (r += i.count * i.ingredient.send(type) / 100).to_f
    end
    r
  end
end
