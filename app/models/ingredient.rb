class Ingredient < ActiveRecord::Base
  has_many :ingredients_counts
  has_many :messes, through: :ingredients_counts
end
