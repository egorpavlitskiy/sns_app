class Menu < ActiveRecord::Base
  belongs_to :week_menu
  belongs_to :mess

  scope :order_by_week_day, -> { order(week_day: :ASC) }
  scope :order_by_position, -> { order(position: :ASC) }
end
