$(document).ready(function () {

    $('#myModal > select').select2({
        dropdownParent: $('#myModal'),
        width: 'auto'
    });

    $('.select2').css({
        width: "auto",
        minWidth: "200px"
    });
});