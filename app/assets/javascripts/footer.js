$(document).ready(function(){
    let modalWindow = $('#myModal');
    let btn = $("#myBtn");
    let span = $(".close");

    btn.on('click', function() {
        modalWindow.css("display", "block")
    });
    span.on('click', function() {
        modalWindow.css("display", "none")
    });
    $(window).on('click', function(event) {
        if ($(event.target).attr('id') === modalWindow.attr('id')) {
            modalWindow.css("display", "none")
        }
    });
});