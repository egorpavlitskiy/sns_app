$(document).ready(function(){
    modalWindow = $('#mess-modal');
    btn = $(".mess-modal-link");
    span = $(".close");

    $('.menu-mess-name').on('click', function (e) {
        e.preventDefault();
        url = $(e.target).attr('href');
        modalWindow.modal({
            remote: url
        })
    });



    modalWindow.on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
        $(this).find('.modal-content').text('')
    })
});