Rails.application.routes.draw do
  devise_for :users
  get 'persons/profile'
  get 'persons/profile', as: 'user_root'
  resources :messes
  resources :menus
  post 'messes/new'
  devise_scope :user do
    authenticated :user do
      root 'persons#profile', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   messes :products

  # Example resource route with options:
  #   messes :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-messes:
  #   messes :products do
  #     messes :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-messes:
  #   messes :products do
  #     messes :comments
  #     messes :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   messes :posts, concerns: :toggleable
  #   messes :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     messes :products
  #   end
end
