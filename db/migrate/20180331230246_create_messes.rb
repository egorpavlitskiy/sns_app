class CreateMesses < ActiveRecord::Migration
  def self.up
    create_table :messes do |t|
      t.string :name, null: false, default: ""
      t.string :description, null: false, default: ""
      t.string :category, null: false, default: ""
      t.integer :cooking_time, default: 0, null: false
      t.timestamps null: false
    end
  end
end
