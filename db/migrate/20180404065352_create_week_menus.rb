class CreateWeekMenus < ActiveRecord::Migration
  def change
    create_table :week_menus do |t|
      # t.belongs_to :menu, index: true
      t.belongs_to :user, index: true
      t.string :name, null: false, default: ""
      t.timestamps null: false
    end
  end
end
