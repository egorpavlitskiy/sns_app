class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.belongs_to :mess, index: true
      t.belongs_to :week_menu, index: true
      t.integer :week_day, default: 0, null: false
      t.integer :position, default: 0, null: false
      t.boolean :active, default: false
      t.timestamps null: false
    end
  end
end
