class CreateIngredients < ActiveRecord::Migration
  def self.up
    create_table :ingredients do |t|
      t.string :name, null: false, default: ""
      t.integer :calories, default: 0, null: false
      t.integer :fat, default: 0, null: false
      t.integer :carbs, default: 0, null: false
      t.integer :protein, default: 0, null: false
      t.timestamps null: false
    end
  end
end
