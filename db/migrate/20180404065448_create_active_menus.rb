class CreateActiveMenus < ActiveRecord::Migration
  def change
    create_table :active_menus do |t|
      t.belongs_to :user, index: true
      t.belongs_to :week_menu, index: true
      t.timestamps null: false
    end
  end
end
