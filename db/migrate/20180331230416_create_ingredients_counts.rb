class CreateIngredientsCounts < ActiveRecord::Migration
  def self.up
    create_table :ingredients_counts do |t|
      t.belongs_to :mess, index: true
      t.belongs_to :ingredient, index: true
      t.integer :count, default: 0, null: false
      t.timestamps null: false
    end
  end
end
