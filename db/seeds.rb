# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Ingredient.create(name: "Ветчина", calories: "278", fat: "20.9", carbs: "0", protein: "22.6")
Ingredient.create(name: "Огурцы", calories: "15", fat: "0", carbs: "3", protein: "0.8")
Ingredient.create(name: "Помидоры", calories: "20", fat: "0.2", carbs: "3.7", protein: "1.0")
Ingredient.create(name: "Сыр", calories: "501", fat: "45", carbs: "0", protein: "24 ")
Ingredient.create(name: "Хлеб", calories: "224", fat: "0.7", carbs: "49.8", protein: "4.7")

# Menu active
Menu.create(mess_id: 2, week_day: 1, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 1, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 1, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 2, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 2, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 3, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 3, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 4, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 4, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 5, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 5, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 5, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 6, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 6, position: 2, week_menu_id: 1)
Menu.create(mess_id: 2, week_day: 7, position: 1, week_menu_id: 1)
Menu.create(mess_id: 3, week_day: 7, position: 2, week_menu_id: 1)
# Menu
Menu.create(mess_id: 1, week_day: 1, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 1, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 2, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 2, position: 2, week_menu_id: 2)
Menu.create(mess_id: 4, week_day: 2, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 3, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 3, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 4, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 4, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 5, position: 1, week_menu_id: 2)
Menu.create(mess_id: 4, week_day: 5, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 5, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 6, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 6, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 7, position: 1, week_menu_id: 2)

Menu.create(mess_id: 1, week_day: 1, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 1, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 2, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 2, position: 2, week_menu_id: 2)
Menu.create(mess_id: 4, week_day: 2, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 3, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 3, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 4, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 4, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 5, position: 1, week_menu_id: 2)
Menu.create(mess_id: 4, week_day: 5, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 5, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 6, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 6, position: 2, week_menu_id: 2)
Menu.create(mess_id: 1, week_day: 7, position: 1, week_menu_id: 2)
Menu.create(mess_id: 5, week_day: 7, position: 2, week_menu_id: 2)

# Week_menu
WeekMenu.create(user_id: 1, name: "Помидоро-огуречное меню")
WeekMenu.create(user_id: 1, name: "Б2утербродное меню")
WeekMenu.create(user_id: 2, name: "Бутербродное меню")


# Active_menu
ActiveMenu.create(user_id: 1, week_menu_id: 1)
ActiveMenu.create(user_id: 2, week_menu_id: 3)
