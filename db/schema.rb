# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180404065448) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_menus", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "week_menu_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "active_menus", ["user_id"], name: "index_active_menus_on_user_id", using: :btree
  add_index "active_menus", ["week_menu_id"], name: "index_active_menus_on_week_menu_id", using: :btree

  create_table "ingredients", force: :cascade do |t|
    t.string   "name",       default: "", null: false
    t.integer  "calories",   default: 0,  null: false
    t.integer  "fat",        default: 0,  null: false
    t.integer  "carbs",      default: 0,  null: false
    t.integer  "protein",    default: 0,  null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "ingredients_counts", force: :cascade do |t|
    t.integer  "mess_id"
    t.integer  "ingredient_id"
    t.integer  "count",         default: 0, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "ingredients_counts", ["ingredient_id"], name: "index_ingredients_counts_on_ingredient_id", using: :btree
  add_index "ingredients_counts", ["mess_id"], name: "index_ingredients_counts_on_mess_id", using: :btree

  create_table "menus", force: :cascade do |t|
    t.integer  "mess_id"
    t.integer  "week_menu_id"
    t.integer  "week_day",     default: 0,     null: false
    t.integer  "position",     default: 0,     null: false
    t.boolean  "active",       default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "menus", ["mess_id"], name: "index_menus_on_mess_id", using: :btree
  add_index "menus", ["week_menu_id"], name: "index_menus_on_week_menu_id", using: :btree

  create_table "messes", force: :cascade do |t|
    t.string   "name",         default: "", null: false
    t.string   "description",  default: "", null: false
    t.string   "category",     default: "", null: false
    t.integer  "cooking_time", default: 0,  null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "week_menus", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",       default: "", null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "week_menus", ["user_id"], name: "index_week_menus_on_user_id", using: :btree

end
